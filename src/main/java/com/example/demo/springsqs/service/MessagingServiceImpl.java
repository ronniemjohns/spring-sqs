package com.example.demo.springsqs.service;

import com.example.demo.springsqs.dto.NameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class MessagingServiceImpl implements MessagingService {

    private static Logger LOG = LoggerFactory.getLogger(MessagingServiceImpl.class);

    @Autowired
    private QueueMessagingTemplate queueMessagingTemplate;

    @Value("${cloud.aws.end-point.uri}")
    private String endpoint;

    @Override
    public void addMessage(NameValuePair message) {
        LOG.info("Adding message {} to queue", message);
        // TODO: create message headers and other helpful info
        queueMessagingTemplate.send(endpoint, MessageBuilder.withPayload(message.getValue()).build());
    }


    @Override
    public void processMessage() {
        LOG.info("Retrieving message");
        Message<?> message = queueMessagingTemplate.receive(endpoint);
        LOG.info("" + message.getPayload());

        LOG.info("Full message details: {}", message);

    }
}
