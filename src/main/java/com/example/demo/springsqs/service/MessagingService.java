package com.example.demo.springsqs.service;

import com.example.demo.springsqs.dto.NameValuePair;

public interface MessagingService {

    void addMessage(NameValuePair message);

    void processMessage();

}
