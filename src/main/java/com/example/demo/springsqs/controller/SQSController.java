package com.example.demo.springsqs.controller;

import com.example.demo.springsqs.dto.NameValuePair;
import com.example.demo.springsqs.service.MessagingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sqs")
public class SQSController {

    @Autowired
    private MessagingService messagingService;

    @PostMapping
    public void createMessage(@RequestBody NameValuePair message) {
        messagingService.addMessage(message);
    }

    @GetMapping
    @SqsListener("TestQueue")
    public void processMessage() {
        messagingService.processMessage();
    }

}
