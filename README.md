---

## AMAZON SQS POC

By using this POC you should be able to test out Amazon SQS.  To use this, a bit of setup needs to take place.

1. terraform must be installed on the system running the project
2. run the docker-compose file in the infra directory (docker-compose up -d)
3. the terraform apply command should be ran in the infra/mod_localstack directory

To validate the queues are functioning, import the postman json in the src/test/postman directory
Start up the service
Click the POST in postman to add a message
Click the GET in postman to retrieve that message.


NOTE:  This project does not use [Spring Cloud Stream](https://spring.io/projects/spring-cloud-stream).  That will be tested in a different project.

Project follows youtube video here: [Spring Cloud AWS | Amazon Simple Queue Service | SQS | Spring Boot | JavaTechie](https://www.youtube.com/watch?v=q3zo3YREfJI), though this uses terraform to fake the SQS queues.

This is not expected to be an example of best practices.  [Please see the Amazon SQS guide for best practices](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-best-practices.html)

---
